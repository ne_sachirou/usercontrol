﻿using System;
using System.Web;

namespace UserControl
{
    public partial class confirm : System.Web.UI.Page
    {
        private static string MainPageUrl = "view.aspx";

        private User _TargetUser;
        private User TargetUser
        {
            get
            {
                if (_TargetUser == null && Session["TargetUser"] != null)
                    _TargetUser = (User)Session["TargetUser"];
                return _TargetUser;
            }
            set {
                Session["TargetUser"] = _TargetUser = value;
            }
        }

        private void ShowError(string message)
        {
            LabelError.Text = HttpUtility.HtmlEncode(message);
            LabelError.Visible = true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (new Login(Session).LoginID == null) Response.Redirect(Login.LoginUrl);
            if (Session["notification"] != null)
            {
                LabelNotification.Text = HttpUtility.HtmlEncode((string)Session["notification"]);
                Session["notification"] = string.Empty;
            }
            else LabelNotification.Text = string.Empty;

            if (TargetUser == null) Response.Redirect(MainPageUrl);
            LabelUserID.Text = HttpUtility.HtmlEncode(TargetUser.ID);
            LabelName.Text = HttpUtility.HtmlEncode(TargetUser.Name);
            if (TargetUser.IsPassResetted())
                LabelPass.Text = HttpUtility.HtmlEncode(TargetUser.Pass);
            LabelAge.Text = TargetUser.Age.ToString();
        }

        protected void ButtonUserForm_Click(object sender, EventArgs e)
        {
            var users = new Users();
            try
            {
                users.UpdateUser(TargetUser);
            }
            catch (InvalidOperationException err)
            {
                ShowError(err.Message);
                return;
            }
            Session["notification"] = string.Format("既存のUser「{0}」の情報を更新しました!", TargetUser.ID);
            TargetUser = null;
            Response.Redirect(MainPageUrl);
        }
    }
}
