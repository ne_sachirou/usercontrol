﻿using System;
using System.Web;

namespace UserControl
{
    public partial class _Default : System.Web.UI.Page
    {
        private void ShowErrorCannotAuth()
        {
            LabelErrorCannotAuth.Visible = true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["notification"] != null)
            {
                LabelNotification.Text = HttpUtility.HtmlEncode((string)Session["notification"]);
                Session["notification"] = string.Empty;
            }
            else LabelNotification.Text = string.Empty;
        }

        protected void ButtonLogin_Click(object sender, EventArgs e)
        {
            var login = new Login(Session);
            string userID = TextBoxUserID.Text;
            if (login.Auth(userID, TextBoxPass.Text)) Response.Redirect("view.aspx");
            else ShowErrorCannotAuth();
        }
    }
}
