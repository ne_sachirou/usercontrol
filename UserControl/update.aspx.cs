﻿using System;
using System.Web;

namespace UserControl
{
    public partial class update : System.Web.UI.Page
    {
        private static string MainPageUrl = "view.aspx";

        private User _TargetUser;
        private User TargetUser
        {
            get
            {
                if (_TargetUser == null)
                {
                    string userID = HttpUtility.UrlDecode(Request.QueryString["id"]);
                    if (userID != null) _TargetUser = new Users().FindUserByID(userID);
                }
                return _TargetUser;
            }
        }

        private void ShowError(string message)
        {
            LabelError.Text = HttpUtility.HtmlEncode(message);
            LabelError.Visible = true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string loginID = new Login(Session).LoginID;
            if (loginID == null) Response.Redirect(Login.LoginUrl);
            if (Session["notification"] != null)
            {
                LabelNotification.Text = HttpUtility.HtmlEncode((string)Session["notification"]);
                Session["notification"] = string.Empty;
            }
            else LabelNotification.Text = string.Empty;

            if (TargetUser == null) Response.Redirect(MainPageUrl);
            if (!IsPostBack)
            {
                if (TargetUser.ID == loginID)
                {
                    MultiViewPass.SetActiveView(ViewPassMine);
                    RegularExpressionValidatorPass.Enabled = true;
                }
                else MultiViewPass.SetActiveView(ViewpassOther);
                LabelUserID.Text = HttpUtility.HtmlEncode(TargetUser.ID);
                TextBoxName.Text = TargetUser.Name;
                TextBoxAge.Text = TargetUser.Age.ToString();
            }
        }

        protected void ButtonUserForm_Click(object sender, EventArgs e)
        {
            uint age;
            if (!uint.TryParse(TextBoxAge.Text, out age)) age = 0;
            try
            {
                TargetUser.Name = TextBoxName.Text;
                TargetUser.Age = age;
                if (MultiViewPass.GetActiveView() == ViewPassMine && TextBoxPass.Text != string.Empty)
                {
                    if (TextBoxCurrentPass.Text != TargetUser.Pass)
                    {
                        ShowError("現在のパスワードが間違っています。");
                        return;
                    }
                    TargetUser.Pass = TextBoxPass.Text;
                }
                else if (!LinkButtonResetPassword.Enabled)
                    TargetUser.ResetPass();
            }
            catch (FormatException err)
            {
                ShowError(err.Message);
                return;
            }
            Session["TargetUser"] = TargetUser;
            Response.Redirect("confirm.aspx");
        }

        protected void LinkButtonResetPassword_Click(object sender, EventArgs e)
        {
            TargetUser.ResetPass();
            LabelPass.Text = TargetUser.Pass;
            LinkButtonResetPassword.Enabled = false;
        }
    }
}
