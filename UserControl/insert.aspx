﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="insert.aspx.cs" Inherits="UserControl.insert" ValidateRequest="False" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>insert | UserControl</title>
    <link rel="Stylesheet" href="style/style.min.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div id="header">
        <h1>追加 | UserControl</h1>
        <div id="headerMenu">
            Menu
            <ul id="headerMenuSlidedown">
                <li class="headerMenuItem"><a href="view.aspx">メイン</a></li>
                <li class="headerMenuItem"><a href="logout.aspx">Logout</a></li>
            </ul>
        </div>
    </div>
    <div id="main">
        <div id="userForm">
            <div class="userFormItem">
                <div class="userFormItemName">ID</div>
                <div class="userFormItemInput">
                    <asp:TextBox ID="TextBoxUserID" runat="server" 
                        ontextchanged="TextBoxUserID_TextChanged" AutoPostBack="True"></asp:TextBox>
                </div>
            </div>
            <div class="userFormItem">
                <div class="userFormItemName">Name</div>
                <div class="userFormItemInput">
                    <asp:TextBox ID="TextBoxName" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="userFormItem">
                <div class="userFormItemName">Pass</div>
                <div class="userFormItemInput">
                    <asp:TextBox ID="TextBoxPass" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="userFormItem">
                <div class="userFormItemName">Age</div>
                <div class="userFormItemInput">
                    <asp:TextBox ID="TextBoxAge" runat="server"></asp:TextBox>
                </div>
            </div>
            <div id="userFormError">
                <asp:Label ID="LabelError" CssClass="error" runat="server" Text="" Visible="False"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorUserID" CssClass="error" runat="server" 
                    ErrorMessage="IDは必須です。" ControlToValidate="TextBoxUserID"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorUserID" 
                    runat="server" ErrorMessage="IDの書式が違います。" 
                    ControlToValidate="TextBoxUserID" CssClass="error" 
                    ValidationExpression="[A-Za-z0-9]{1,5}"></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorName" 
                    runat="server" ErrorMessage="名前の書式が違います。" ControlToValidate="TextBoxName" 
                    ValidationExpression=".{1,20}" CssClass="error"></asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorPass" runat="server" 
                    ErrorMessage="パスワードは必須です。" ControlToValidate="TextBoxPass" 
                    CssClass="error"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorPass" 
                    runat="server" ErrorMessage="パスワードの書式が違います。" 
                    ControlToValidate="TextBoxPass" CssClass="error" 
                    ValidationExpression="[A-Za-z0-9]{1,3}"></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorAge" 
                    runat="server" ErrorMessage="年齢の書式が違います。" ControlToValidate="TextBoxAge" 
                    ValidationExpression="[1-9][0-9]*" CssClass="error"></asp:RegularExpressionValidator>
            </div>
            <asp:Button ID="ButtonUserForm" runat="server" Text="追加" 
                onclick="ButtonUserForm_Click" style="height: 21px" />
        </div>
        <div id="footer">2012 ne_Sachirou</div>
    </div>
    </form>
    <script src="script/script.min.js" type="text/javascript"></script>
</body>
</html>
