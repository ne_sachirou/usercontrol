﻿using System;
using System.Web;

namespace UserControl
{
    public partial class insert : System.Web.UI.Page
    {
        private void ShowErrorUserIDIsAlreadyExist()
        {
            LabelError.Text = "そのIDは既に別のuserに割り当てられています。";
            LabelError.Visible = true;
        }

        private void HideErrorUserIDIsAlreadyExist()
        {
            LabelError.Visible = false;
        }

        private void ShowErrorIllegalFormat(string message)
        {
            LabelError.Text = HttpUtility.HtmlEncode(message);
            LabelError.Visible = true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (new Login(Session).LoginID == null) Response.Redirect(Login.LoginUrl);
        }

        protected void ButtonUserForm_Click(object sender, EventArgs e)
        {
            string userID = TextBoxUserID.Text;
            uint age;
            if (!uint.TryParse(TextBoxAge.Text, out age)) age = 0;
            try
            {
                User user = new User(userID, TextBoxName.Text, TextBoxPass.Text, age);
                new Users().AddUser(user);
            }
            catch (FormatException err)
            {
                ShowErrorIllegalFormat(err.Message);
                return;
            }
            catch (InvalidOperationException)
            {
                ShowErrorUserIDIsAlreadyExist();
                return;
            }
            Session["notification"] = string.Format("新たなUser「{0}」を追加しました!", userID);
            Response.Redirect("view.aspx");
        }

        protected void TextBoxUserID_TextChanged(object sender, EventArgs e)
        {
            if (new Users().IsExistID(TextBoxUserID.Text)) ShowErrorUserIDIsAlreadyExist();
            else HideErrorUserIDIsAlreadyExist();
        }
    }
}
