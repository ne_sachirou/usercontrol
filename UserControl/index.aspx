﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="UserControl._Default" ValidateRequest="False" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>UserControl</title>
    <link rel="Stylesheet" href="style/style.min.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div id="header"><h1>UserControl</h1></div>
    <asp:Label ID="LabelNotification" runat="server" Text=""></asp:Label>
    <div id="main">
        <div id="loginForm">
            <div class="loginFormItem">
                <div class="loginFormItemName">ID</div>
                <div class="loginFormItemInput">
                    <asp:TextBox ID="TextBoxUserID" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="loginFormItem">
                <div class="loginFormItemName">Pass</div>
                <div class="loginFormItemInput">
                    <asp:TextBox ID="TextBoxPass" runat="server" TextMode="Password"></asp:TextBox>
                </div>
            </div>
            <div id="loginFormError">
                <asp:Label ID="LabelErrorCannotAuth" runat="server" Text="IDとパスワードが正しくありません。" CssClass="error" Visible="False"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorUserID" runat="server" 
                    ErrorMessage="IDは必須です。" ControlToValidate="TextBoxUserID" CssClass="error"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorUserID" 
                    runat="server" ErrorMessage="IDの書式が違います。" ControlToValidate="TextBoxUserID" 
                    ValidationExpression="[A-Za-z0-9]{1,5}" CssClass="error"></asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorPass" runat="server" 
                    ErrorMessage="パスワードは必須です。" ControlToValidate="TextBoxPass" CssClass="error"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorPass" 
                    runat="server" ErrorMessage="パスワードの書式が違います。" 
                    ControlToValidate="TextBoxPass" ValidationExpression="[A-Za-z0-9]{1,3}" CssClass="error"></asp:RegularExpressionValidator>
            </div>
            <asp:Button ID="ButtonLogin" runat="server" Text="Login" 
                onclick="ButtonLogin_Click" />
        </div>
        <div id="footer">2012 ne_Sachirou</div>
    </div>
    </form>
    <script src="script/script.min.js" type="text/javascript"></script>
</body>
</html>
