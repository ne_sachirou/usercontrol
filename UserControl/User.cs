﻿using System;
using System.Text.RegularExpressions;

namespace UserControl
{
    /// <summary>
    /// User Entity
    /// </summary>
    public class User
    {
        private string _ID;
        public string ID {
            get { return _ID; }
            /// <summary>
            /// 5字以内の英数文字列である必要があります。空文字列は受け付けられません。違反していればFormatExceptionを投げます。
            /// </summary>
            set
            {
                if (value == string.Empty || value.Length > 5 || !Regex.IsMatch(value, "[A-Za-z0-9]{1,5}"))
                    throw new FormatException("ID has illegal format.");
                _ID = value;
            }
        }

        private string _Name;
        public string Name {
            get { return _Name; }
            /// <summary>
            /// 20字以内の文字列である必要があります。空文字列は受け付けられません。違反していればFormatExceptionを投げます。
            /// </summary>
            set
            {
                if (value == string.Empty || value.Length > 20)
                    throw new FormatException("Name has illegal format.");
                _Name = value;
            }
        }

        public static readonly string DefaultPass = "000";
        private string _Pass;
        public string Pass
        {
            get { return _Pass; }
            /// <summary>
            /// 3字以内の英数文字列である必要があります。空文字列は受け付けられません。違反していればFormatExceptionを投げます。
            /// </summary>
            set
            {
                if (value == string.Empty || value.Length > 3 || !Regex.IsMatch(value, "[A-Za-z0-9]{1,3}"))
                    throw new FormatException("Pass has illegal format.");
                _Pass = value;
            }
        }

        public static readonly uint DefaultAge = 0;
        public uint Age { get; set; }

        /// <summary>
        /// 各parameterの書式が正しい必要があります。違反していればFormatExceptionを投げます。
        /// </summary>
        /// <param name="id">5字以内の英数文字列である必要があります。空文字列は受け付けられません。</param>
        /// <param name="name">20字以内の文字列である必要があります。空文字列は受け付けられません。</param>
        /// <param name="pass">3字以内の英数文字列である必要があります。空文字列は受け付けられません。</param>
        /// <param name="age"></param>
        public User(string id, string name, string pass, uint age)
        {
            ID = id;
            Name = name;
            Pass = pass;
            Age = age;
        }

        /// <summary>
        /// 各parameterの書式が正しい必要があります。違反していればFormatExceptionを投げます。
        /// </summary>
        /// <param name="id">5字以内の英数文字列である必要があります。空文字列は受け付けられません。</param>
        /// <param name="name">20字以内の文字列である必要があります。空文字列は受け付けられません。</param>
        /// <param name="pass">3字以内の英数文字列である必要があります。空文字列は受け付けられません。</param>
        public User(string id, string name, string pass)
        {
            ID = id;
            Name = name;
            Pass = pass;
            Age = DefaultAge;
        }

        public void ResetPass()
        {
            Pass = DefaultPass;
        }

        public bool IsPassResetted()
        {
            return Pass == DefaultPass;
        }
    }
}
