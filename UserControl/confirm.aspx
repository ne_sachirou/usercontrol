﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="confirm.aspx.cs" Inherits="UserControl.confirm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>confirm | UserControl</title>
    <link rel="Stylesheet" href="style/style.min.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div id="header">
        <h1>確認 | UserControl</h1>
        <div id="headerMenu">
            Menu
            <ul id="headerMenuSlidedown">
                <li class="headerMenuItem"><a href="view.aspx">メイン</a></li>
                <li class="headerMenuItem"><a href="logout.aspx">Logout</a></li>
            </ul>
        </div>
    </div>
    <asp:Label ID="LabelNotification" runat="server" Text=""></asp:Label>
    <div id="main">
        <div id="userForm">
            <div class="userFormItem">
                <div class="userFormItemName">ID</div>
                <div class="userFormItemInput">
                    <asp:Label ID="LabelUserID" runat="server" Text=""></asp:Label>
                </div>
            </div>
            <div class="userFormItem">
                <div class="userFormItemName">Name</div>
                <div class="userFormItemInput">
                    <asp:Label ID="LabelName" runat="server" Text=""></asp:Label>
                </div>
            </div>
            <div class="userFormItem">
                <div class="userFormItemName">Pass</div>
                <div class="userFormItemInput">
                    <asp:Label ID="LabelPass" runat="server" Text="********"></asp:Label>
                </div>
            </div>
            <div class="userFormItem">
                <div class="userFormItemName">Age</div>
                <div class="userFormItemInput">
                    <asp:Label ID="LabelAge" runat="server" Text=""></asp:Label>
                </div>
            </div>
            <div id="userFormError">
                <asp:Label ID="LabelError" CssClass="error" runat="server" Text="" Visible="False"></asp:Label>
            </div>
            <asp:Button ID="ButtonUserForm" runat="server" Text="OK" 
                onclick="ButtonUserForm_Click" />
        </div>
        <div id="footer">2012 ne_Sachirou</div>
    </div>
    </form>
    <script src="script/script.min.js" type="text/javascript"></script>
</body>
</html>
