﻿
namespace UserControl
{
    /// <summary>
    /// Login Proxy
    /// </summary>
    public class Login
    {
        public static string LoginUrl = "index.aspx";
        private Users Users;
        private System.Web.SessionState.HttpSessionState Session;

        /// <summary>
        /// login idを示します。loginしていない場合にはnullを返します。
        /// </summary>
        public string LoginID
        {
            get
            {
                return Session["id"] != null ? (string)Session["id"] : null;
            }
        }

        public Login(System.Web.SessionState.HttpSessionState session)
        {
            Users = new Users();
            Session = session;
        }

        /// <summary>
        /// userIDとpassが正当か否か確認します。正当であれば、Sessionのlogin処理を行います。
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="pass"></param>
        /// <returns></returns>
        public bool Auth(string userID, string pass)
        {
            if (Users.Auth(userID, pass))
            {
                Session["id"] = userID;
                return true;
            }
            else
            {
                Session["notification"] = "Loginしてください。";
                return false;
            }
        }

        /// <summary>
        /// Sessionをlog outさせます。
        /// </summary>
        public void Logout()
        {
            Session["id"] = null;
        }
    }
}
