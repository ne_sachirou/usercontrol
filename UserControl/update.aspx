﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="update.aspx.cs" Inherits="UserControl.update" ValidateRequest="False" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>update | UserControl</title>
    <link rel="Stylesheet" href="style/style.min.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div id="header">
        <h1>修正 | UserControl</h1>
        <div id="headerMenu">
            Menu
            <ul id="headerMenuSlidedown">
                <li class="headerMenuItem"><a href="view.aspx">メイン</a></li>
                <li class="headerMenuItem"><a href="logout.aspx">Logout</a></li>
            </ul>
        </div>
    </div>
    <asp:Label ID="LabelNotification" runat="server" Text=""></asp:Label>
    <div id="main">
        <div id="userForm">
            <div class="userFormItem">
                <div class="userFormItemName">ID</div>
                <div class="userFormItemInput">
                    <asp:Label ID="LabelUserID" runat="server" Text=""></asp:Label>
                </div>
            </div>
            <div class="userFormItem">
                <div class="userFormItemName">Name</div>
                <div class="userFormItemInput">
                    <asp:TextBox ID="TextBoxName" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="userFormItem">
                <div class="userFormItemName">Pass</div>
                <div class="userFormItemInput">
                    <asp:MultiView ID="MultiViewPass" runat="server">
                        <asp:View ID="ViewpassOther" runat="server">
                            <asp:Label ID="LabelPass" runat="server" Text="********"></asp:Label>
                            <asp:LinkButton ID="LinkButtonResetPassword" runat="server" 
                                onclick="LinkButtonResetPassword_Click">Reset Password</asp:LinkButton>
                        </asp:View>
                        <asp:View ID="ViewPassMine" runat="server">
                            <div>
                                現在のパスワード
                                <asp:TextBox ID="TextBoxCurrentPass" runat="server" TextMode="Password"></asp:TextBox>
                            </div>
                            <div>
                                新しいパスワード
                                <asp:TextBox ID="TextBoxPass" runat="server"></asp:TextBox>
                            </div>
                        </asp:View>
                    </asp:MultiView>
                </div>
            </div>
            <div class="userFormItem">
                <div class="userFormItemName">Age</div>
                <div class="userFormItemInput">
                    <asp:TextBox ID="TextBoxAge" runat="server"></asp:TextBox>
                </div>
            </div>
            <div id="userFormError">
                <asp:Label ID="LabelError" CssClass="error" runat="server" Text="" Visible="False"></asp:Label>
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorName" 
                    runat="server" ErrorMessage="名前の書式が違います。" ControlToValidate="TextBoxName" 
                    ValidationExpression=".{1,20}" CssClass="error"></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorPass" 
                    runat="server" ErrorMessage="新しいパスワードの書式が違います。" 
                    ControlToValidate="TextBoxPass" CssClass="error" 
                    ValidationExpression="[A-Za-z0-9]{1,3}" Enabled="False"></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorAge" 
                    runat="server" ErrorMessage="年齢の書式が違います。" ControlToValidate="TextBoxAge" 
                    ValidationExpression="[1-9][0-9]*" CssClass="error"></asp:RegularExpressionValidator>
            </div>
            <asp:Button ID="ButtonUserForm" runat="server" Text="修正" 
            onclick="ButtonUserForm_Click" />
        </div>
        <div id="footer">2012 ne_Sachirou</div>
    </div>
    </form>
    <script src="script/script.min.js" type="text/javascript"></script>
</body>
</html>
