﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="view.aspx.cs" Inherits="UserControl.view" ValidateRequest="False" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>view | UserControl</title>
    <link rel="Stylesheet" href="style/style.min.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div id="header">
        <h1>一覧 | UserControl</h1>
        <div id="headerMenu">
            Menu
            <ul id="headerMenuSlidedown">
                <li class="headerMenuItem"><a href="view.aspx">メイン</a></li>
                <li class="headerMenuItem"><a href="logout.aspx">Logout</a></li>
            </ul>
        </div>
    </div>
    <asp:Label ID="LabelNotification" runat="server" Text=""></asp:Label>
    <div id="main">
        <div>こんにちは<asp:Literal ID="LiteralCurrentUserName" runat="server"></asp:Literal>さん</div>
        <div id="usersViewSearch">
            <asp:TextBox ID="TextBoxSearch" runat="server"></asp:TextBox>
            <asp:Button ID="ButtonSearch" runat="server" Text="検索" 
                onclick="ButtonSearch_Click" />
        </div>
        <div id="usersViewAdd"><a href="insert.aspx">追加</a></div>
        <table id="usersView">
            <tr>
                <th>ID
                    <asp:LinkButton ID="LinkButtonOrderUserID" runat="server" 
                        CssClass="usersViewOrder" onclick="LinkButtonOrderUserID_Click">▼</asp:LinkButton></th>
                <th>Name 
                    <asp:LinkButton ID="LinkButtonOrderName" runat="server" 
                        CssClass="usersViewOrder" onclick="LinkButtonOrderName_Click">▽</asp:LinkButton></th>
                <th>Age 
                    <asp:LinkButton ID="LinkButtonOrderAge" runat="server" 
                        CssClass="usersViewOrder" onclick="LinkButtonOrderAge_Click">▽</asp:LinkButton></th>
                <th>修正</th>
                <th>削除</th>
            </tr>
            <asp:Literal ID="LiteralUsersView" runat="server"></asp:Literal>
        </table>
        <asp:Label ID="LabelPageNumbers" runat="server" Text=""></asp:Label>
        <div id="footer">2012 ne_Sachirou</div>
    </div>
    </form>
    <script src="script/script.min.js" type="text/javascript"></script>
</body>
</html>
