﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;

namespace UserControl
{
    /// <summary>
    /// 
    /// </summary>
    public class Users
    {
        private static string TableName = "user";
        public uint Length = 10;
        public uint Page = 1;

        public string Search { get; set; }
        public string[] SearchWords
        {
            get
            {
                if (Search == null) return new string[] { "" };
                string[] words = Search.Split(new char[] { ' ' });
                for (int i = 0; i < words.Length; ++i) words[i] = words[i].ToLower();
                return words;
            }
        }

        private string _Order;
        /// <summary>
        /// This must be "id" || "name" || "age".
        /// </summary>
        public string Order
        {
            get { return _Order; }
            set
            {
                if (value != "id" && value != "name" && value != "age") value = "id";
                _Order = value;
            }
        }

        private string _Direction;
        /// <summary>
        /// This must be "asc" || "desc".
        /// </summary>
        public string Direction
        {
            get { return _Direction; }
            set
            {
                if (value != "asc" && value != "desc") value = "asc";
                _Direction = value;
            }
        }

        public Users() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryString">This may give as Request.QueryString</param>
        public Users(System.Collections.Specialized.NameValueCollection queryString)
        {
            uint page;
            if (uint.TryParse(queryString["page"], out page)) Page = page;
            Search = HttpUtility.UrlDecode(queryString["search"]);
            Order = queryString["order"];
            Direction = queryString["dir"];
        }

        /// <summary>
        /// 指定したIDのUserが既に存在するか否かを判定します。
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public bool IsExistID(string userID)
        {
            var db = DBConnect.GetInstance();
            return db.Count(TableName, "id = ?ID", new Dictionary<string, object>() { { "ID", userID } }) > 0;
        }

        /// <summary>
        /// 指定されたIDを持つUserを返します。IDが見付からない場合はnullを返します。
        /// IDが既に存在するか否かを確かめるだけの為には、<see cref="IsExistID"/>を使用してください。
        /// </summary>
        /// <param name="userID"></param>
        /// <returns>User or null</returns>
        public User FindUserByID(string userID)
        {
            User user = null;
            var db = DBConnect.GetInstance();
            using (var connection = db.GetConnection())
            {
                connection.Open();
                using(var reader = db.Select(connection, TableName, new string[] { "id", "name", "pass", "age" },
                    "id = ?ID", new Dictionary<string, object>() { { "ID", userID } }, "id", "asc", 0, 1))
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        user = new User(reader.GetString("id"),
                            reader.GetString("name"),
                            reader.GetString("pass"),
                            reader.GetUInt32("age"));
                    }
                }
            }
            return user;
        }

        /// <summary>
        /// Userの総行数を返します。Searchがnullでない場合は、検索文字列でfilterされた後のUserの総行数を返します。
        /// </summary>
        /// <returns></returns>
        public int UserNumber()
        {
            var db = DBConnect.GetInstance();
            int count;
            if (Search == null) count = db.Count(TableName);
            else count = db.CountWithSearch(TableName, new string[] { "id", "name", "age" }, SearchWords);
            return count;
        }

        /// <summary>
        /// データベースへ新規ユーザを追加します。user IDが既に存在した場合にはInvalidOperationExceptionを投げます。
        /// </summary>
        /// <param name="user"></param>
        public void AddUser(User user)
        {
            if (IsExistID(user.ID))
                throw new InvalidOperationException(string.Format("user id ({0}) is already exist.", user.ID));
            var db = DBConnect.GetInstance();
            db.Insert(TableName, new string[] { "id", "name", "pass", "age" },
                new object[] { user.ID, user.Name, user.Pass, user.Age });
        }

        /// <summary>
        /// Userのデータを更新します。user IDが存在しない場合にはInvalidOperationExceptionを投げます。
        /// </summary>
        /// <param name="user"></param>
        public void UpdateUser(User user)
        {
            if (!IsExistID(user.ID))
                throw new InvalidOperationException(string.Format("user id ({0}) is not found.", user.ID));
            var db = DBConnect.GetInstance();
            db.Update(TableName, new string[] { "name", "pass", "age" }, new object[] { user.Name, user.Pass, user.Age },
                "id = ?ID", new Dictionary<string, object>() { { "ID", user.ID } });
        }

        /// <summary>
        /// Userをデータベースから削除します。user IDが存在しない場合にはInvalidOperationExceptionを投げます。
        /// </summary>
        /// <param name="userID"></param>
        public void DeleteUser(string userID)
        {
            if (!IsExistID(userID))
                throw new InvalidOperationException(string.Format("user id ({0}) is not found.", userID));
            var db = DBConnect.GetInstance();
            db.Delete(TableName, "id = ?ID", new Dictionary<string, object>() { { "ID", userID } });
        }

        /// <summary>
        /// userIDとpassが正当であるか確かめます。
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="pass"></param>
        /// <returns></returns>
        public bool Auth(string userID, string pass)
        {
            var db = DBConnect.GetInstance();
            return db.Count(TableName, "id = ?ID And pass = ?PASS", new Dictionary<string, object>() {
                { "ID", userID },
                { "PASS", pass }
            }) > 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetViewQuery()
        {
            string query = string.Empty;
            if (Page != 0) query += "page=" + Page.ToString();
            if (Search != null)
            {
                if (query != string.Empty) query += "&";
                query += "search=" + HttpUtility.UrlEncode(Search);
            }
            if (Order != null)
            {
                if (query != string.Empty) query += "&";
                query += "order=" + Order;
            }
            if (Order != null && Direction != null)
            {
                if (query != string.Empty) query += "&";
                query += "dir=" + Direction;
            }
            if (query != string.Empty) query = "?" + query;
            return query;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<User> GetUsers()
        {
            var users = new List<User>();
            var columns = new string[] { "id", "name", "pass", "age" };
            var db = DBConnect.GetInstance();
            using (var connection = db.GetConnection())
            {
                connection.Open();
                MySql.Data.MySqlClient.MySqlDataReader reader;
                if (Search != null || Order != null)
                {
                    reader = db.SelectWithSearch(connection, TableName, columns, SearchWords, Order, Direction, (Page - 1) * Length, Length);
                }
                else
                {
                    reader = db.Select(connection, TableName, columns, (Page - 1) * Length, Length);
                }
                using (reader)
                {
                    while (reader.Read())
                    {
                        users.Add(new User(reader.GetString(0),
                            reader.GetString(1),
                            reader.GetString(2),
                            reader.GetUInt32(3)));
                    }
                }
            }
            return users;
        }
    }
}
