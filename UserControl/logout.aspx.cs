﻿using System;

namespace UserControl
{
    public partial class logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            new Login(Session).Logout();
            Response.Redirect("index.aspx");
        }
    }
}
