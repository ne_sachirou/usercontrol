﻿using System;
using System.Web;

namespace UserControl
{
    public partial class delete : System.Web.UI.Page
    {
        private static string MainPageUrl = "view.aspx";

        private string _LoginID;
        private string LoginID
        {
            get
            {
                if (_LoginID == null) _LoginID = new Login(Session).LoginID;
                return _LoginID;
            }
        }

        private string _TargetUserID;
        private string TargetUserID
        {
            get
            {
                if (_TargetUserID == null) _TargetUserID = HttpUtility.UrlDecode(Request.QueryString["id"]);
                return _TargetUserID;
            }
        }

        private void ShowError(string message)
        {
            LabelError.Text = HttpUtility.HtmlEncode(message);
            LabelError.Visible = true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (LoginID == null) Response.Redirect(Login.LoginUrl);
            if (Session["notification"] != null)
            {
                LabelNotification.Text = HttpUtility.HtmlEncode((string)Session["notification"]);
                Session["notification"] = string.Empty;
            }
            else LabelNotification.Text = string.Empty;

            if (TargetUserID == null) Response.Redirect(MainPageUrl);
            User user = new Users().FindUserByID(TargetUserID);
            if (user == null) Response.Redirect(MainPageUrl);
            LabelUserID.Text = HttpUtility.HtmlEncode(TargetUserID);
            LabelName.Text = HttpUtility.HtmlEncode(user.Name);
            LabelPass.Text = "********";
            LabelAge.Text = user.Age.ToString();
            if (LoginID == TargetUserID) ButtonUserForm.Enabled = false;
        }

        protected void ButtonUserForm_Click(object sender, EventArgs e)
        {
            if (LoginID == TargetUserID)
            {
                ShowError("自分のアカウントを消すことはできません。");
                return;
            }
            try
            {
                new Users().DeleteUser(TargetUserID);
            }
            catch (InvalidOperationException err)
            {
                ShowError(err.Message);
                return;
            }
            Session["notification"] = string.Format("User「{0}」を削除しました!", TargetUserID);
            Response.Redirect(MainPageUrl);
        }
    }
}
