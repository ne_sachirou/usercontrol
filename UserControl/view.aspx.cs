﻿using System;
using System.Collections.Generic;
using System.Web;

namespace UserControl
{
    public partial class view : System.Web.UI.Page
    {
        private const string Ascending = "asc";
        private const string Descending = "desc";

        private Users _RequestUsers;
        private Users RequestUsers
        {
            get
            {
                if (_RequestUsers == null) _RequestUsers = new Users(Request.QueryString);
                return _RequestUsers;
            }
        }

        private void ChangeOrder(string order)
        {
            if (RequestUsers.Order == order)
            {
                if (RequestUsers.Direction == Ascending) RequestUsers.Direction = Descending;
                else RequestUsers.Direction = Ascending;
            }
            else
            {
                RequestUsers.Order = order;
                RequestUsers.Direction = Ascending;
            }
            Response.Redirect("view.aspx" + RequestUsers.GetViewQuery());
        }

        private void ShowPageNumber(uint lastPage)
        {
            uint currentPage = RequestUsers.Page;
            string fragment = string.Empty;
            uint startPage = 1;
            if (lastPage > 10)
            {
                startPage = currentPage - 4 > 0 ? currentPage - 4 : 1;
                lastPage = startPage + 9;
            }
            for (uint i = startPage; i <= lastPage; ++i)
            {
                RequestUsers.Page = i;
                if (i == currentPage) fragment += string.Format("<span class=\"pageNumber currentPageNumber\">{0}</span>", i);
                else fragment += string.Format("<a class=\"pageNumber\" href=\"view.aspx{0}\">{1}</a>",
                    HttpUtility.HtmlEncode(RequestUsers.GetViewQuery()),
                    i);
            }
            RequestUsers.Page = currentPage;
            LabelPageNumbers.Text = fragment;
        }

        private void ShowUsersView()
        {
            string fragment = string.Empty;
            foreach (var user in RequestUsers.GetUsers())
            {
                fragment += string.Format(@"<tr><td>{0}</td><td>{1}</td><td>{2}</td>
                    <td><a href=""update.aspx?id={3}"">修正</a></td><td><a href=""delete.aspx?id={3}"">削除</a></td></tr>",
                    HttpUtility.HtmlEncode(user.ID),
                    HttpUtility.HtmlEncode(user.Name),
                    user.Age,
                    HttpUtility.UrlEncode(user.ID));
            }
            LiteralUsersView.Text = fragment;
        }

        private void ShowOrderMark()
        {
            const string blackBottomArrow = "▼";
            const string blackUpArrow = "▲";
            const string whiteBottomArrow = "▽";
            switch (RequestUsers.Order)
            {
                case "id":
                    if (RequestUsers.Direction == Ascending) LinkButtonOrderUserID.Text = blackBottomArrow;
                    else LinkButtonOrderUserID.Text = blackUpArrow;
                    LinkButtonOrderName.Text = whiteBottomArrow;
                    LinkButtonOrderAge.Text = whiteBottomArrow;
                    break;
                case "name":
                    LinkButtonOrderUserID.Text = whiteBottomArrow;
                    if (RequestUsers.Direction == Ascending) LinkButtonOrderName.Text = blackBottomArrow;
                    else LinkButtonOrderName.Text = blackUpArrow;
                    LinkButtonOrderAge.Text = whiteBottomArrow;
                    break;
                case "age":
                    LinkButtonOrderUserID.Text = whiteBottomArrow;
                    LinkButtonOrderName.Text = whiteBottomArrow;
                    if (RequestUsers.Direction == Ascending) LinkButtonOrderAge.Text = blackBottomArrow;
                    else LinkButtonOrderAge.Text = blackUpArrow;
                    break;
                default:
                    LinkButtonOrderUserID.Text = blackBottomArrow;
                    LinkButtonOrderName.Text = whiteBottomArrow;
                    LinkButtonOrderAge.Text = whiteBottomArrow;
                    break;
            }
        }

        private void ShowContents(string loginID)
        {
            LiteralCurrentUserName.Text = HttpUtility.HtmlEncode(RequestUsers.FindUserByID(loginID).Name);
            ShowUsersView();
            if (RequestUsers.Search != null) TextBoxSearch.Text = RequestUsers.Search;
            ShowOrderMark();
            int count = RequestUsers.UserNumber();
            ShowPageNumber(Convert.ToUInt32(count % 10 == 0 ? count / 10 : count / 10 + 1));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string loginID = new Login(Session).LoginID;
            if (loginID == null) Response.Redirect(Login.LoginUrl);
            if (Session["notification"] != null)
            {
                LabelNotification.Text = HttpUtility.HtmlEncode((string)Session["notification"]);
                Session["notification"] = string.Empty;
            }
            else LabelNotification.Text = string.Empty;
            if (new Users().FindUserByID(loginID).IsPassResetted())
            {
                Session["notification"] = "パスワードを変更してください。";
                Response.Redirect(string.Format("update.aspx?id={0}", HttpUtility.HtmlEncode(loginID)));
            }
            
            if (!IsPostBack) ShowContents(loginID);
        }

        protected void ButtonSearch_Click(object sender, EventArgs e)
        {
            RequestUsers.Search = TextBoxSearch.Text;
            Response.Redirect("view.aspx" + RequestUsers.GetViewQuery());
        }

        protected void LinkButtonOrderUserID_Click(object sender, EventArgs e)
        {
            ChangeOrder("id");
        }

        protected void LinkButtonOrderName_Click(object sender, EventArgs e)
        {
            ChangeOrder("name");
        }

        protected void LinkButtonOrderAge_Click(object sender, EventArgs e)
        {
            ChangeOrder("age");
        }
    }
}
