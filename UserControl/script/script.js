(function (_window, _document) {

var requestAnimationFrame, cancelAnimationFrame;

if (_window.requestAnimationFrame && _window.cancelAnimationFrame) {
  requestAnimationFrame = _window.requestAnimationFrame;
  cancelAnimationFrame = _window.cancelAnimationFrame;
} else if (_window.mozRequestAnimationFrame &&
           _window.mozCancelAnimationFrame) {
  requestAnimationFrame = _window.mozRequestAnimationFrame;
  cancelAnimationFrame = _window.mozCancelAnimationFrame;
} else if (_window.msRequestAnimationFrame &&
           _window.msCancelAnimationFrame) {
  requestAnimationFrame = _window.msRequestAnimationFrame;
  cancelAnimationFrame = _window.msCancelAnimationFrame;
} else if (_window.webkitRequestAnimationFrame &&
           _window.webkitCancelAnimationFrame) {
  requestAnimationFrame = _window.webkitRequestAnimationFrame;
  cancelAnimationFrame = _window.webkitCancelAnimationFrame;
} else if (_window.oRequestAnimationFrame &&
           _window.oCancelAnimationFrame) {
  requestAnimationFrame = _window.oRequestAnimationFrame;
  cancelAnimationFrame = _window.oCancelAnimationFrame;
}

if (!String.prototype.trim) {
  /**
   * ECMAScript5 String trim().
   * @return String
   */
  String.prototype.trim = function () {
    return this.replace(/^\s+/, '').replace(/\s+$/, '');
    //return this.replace(/(?:^\s*$)|(?:^\s*((?:[^\s]*\s*)*[^\s])\s*$)/, '$1');
  };
}

/**
 * @param {Function} callback
 * @return {Function} Stop animation function.
 */
function animate (callback) {
  var timerID, stop, loop;

  if (requestAnimationFrame) {
    loop = function () {
      callback();
      requestAnimationFrame(loop);
    };
    timerID = requestAnimationFrame(loop);
    stop = function () { cancelAnimationFrame(timerID); };
  } else {
    loop = function () { callback(); };
    timerID = setInterval(loop, 16);
    stop = function () { clearInterval(timerID); };
  }
  return stop;
}

/**
 * Fadeout the node.
 * @param {Element} node
 */
function hideFadeout (node) {
  var stop,
      opacity = parseFloat(node.style.opacity) || 1;

  stop = animate(function () {
    opacity -= 0.01;
    node.style.opacity = opacity;
    if (opacity <= 0) {
      stop();
      node.style.display = 'none';
    }
  });
}

/**
 * Slidedown to show the node.
 * @param {Element} node
 */
function showSlidedown (node) {
  var stop,
      height = 0;

  node.style.display = 'block';
  node.style.overflow = 'hidden';
  node.style.height = '0%';
  stop = animate(function () {
    height += 10;
    node.style.height = height + '%';
    if (height >= 100) {
      stop();
    }
  });
}

/**
 * Slideup to hide the node.
 * @param {Element} node
 */
function hideSlideup (node) {
  var stop,
      height = parseInt(_window.getComputedStyle(node).height),
      step = ~~(height / 10);

  node.style.overflow = 'hidden';
  stop = animate(function () {
    height -= step;
    node.style.height = height + 'px';
    if (height <= 0) {
      stop();
      node.style.display = 'none';
    }
  });
}


var nodeHeaderMenu = _document.getElementById('headerMenu'),
    nodeHeaderMenuSlidedown = _document.getElementById('headerMenuSlidedown');

function notify () {
  var notifyNode = _document.getElementById('LabelNotification');

  if (!notifyNode || notifyNode.innerHTML.trim() === '') return;
  notifyNode.style.display = 'block';
  notifyNode.style.opacity = 0.9;
  _window.setTimeout(function () { hideFadeout(notifyNode); }, 2000);
}

function toggleHeaderMenu () {
  if (nodeHeaderMenuSlidedown.style.display === 'block') {
    hideSlideup(nodeHeaderMenuSlidedown);
  } else {
    showSlidedown(nodeHeaderMenuSlidedown);
  }
}

notify();
nodeHeaderMenu.addEventListener('click', toggleHeaderMenu);

}(window, document));
